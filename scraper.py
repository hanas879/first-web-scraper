import requests
import smtplib
import math
import time
import os
from bs4 import BeautifulSoup
from datetime import datetime

#PLEASE EDIT TO FIT YOU'R SETUP
product = "https://www.komplett.no/product/898464/pc-nettbrett/pc-baerbar-laptop/chromebook/acer-chromebook-r13-cb5-312t-133-fhd"
price_goal = 4000
headers = {"User-Agent": "YOU'R USER-AGENT"}
send_from = "SEND_FROM"
mail_password = "PASSWORD"
send_to = "SEND_TO"
wait_duration = 3600 #IN SECONDS



def check_price(URL):
    page = requests.get(URL, headers=headers)
    
    soup = BeautifulSoup(page.content, 'html.parser')

    title = soup.find("h1", {"class":"product-main-info-webtext1"}).get_text()
    price = soup.find("span", {"class":"product-price-now"}).get_text()
    converted_price = math.floor(float(price.replace(price[1:2], '')[0:4]))

    if(converted_price <= price_goal):
        print("Price dropped!")
        print(datetime.now().strftime("%d-%m-%Y - %H:%M:%S: ") + title.strip() + " is currently selling for: " + str(converted_price) + ",-")
        send_mail(URL)
    else:
        log = open("log.txt", "a")
        log.write(datetime.now().strftime("%d-%m-%Y - %H:%M:%S: ") + title.strip() + " is currently selling for: " + str(converted_price) + ",-" + "\n")
        log.close()
        print(datetime.now().strftime("%d-%m-%Y - %H:%M:%S: ") + title.strip() + " is currently selling for: " + str(converted_price) + ",-")

def send_mail(URL):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.ehlo

    server.login(send_from, mail_password)

    subject = "Price dropped!"
    body = "Check the link: \n\n" + URL
    msg = f"Subject:  {subject}\n\n{body}"   

    server.sendmail(
        send_from,
        send_to,
        msg
    )

    server.quit()
    print("EMAIL SENDT!")

while True:
    check_price(product)
    time.sleep(wait_duration)